@extends('layouts.app')

@section('content')

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Student Name ({{ $getUser->name }} {{ $getUser->last_name }})</h1>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          @include('_message')
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Student Subject</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body p-0">
              <table class="table table-striped">
                <thead>
                  <tr>
                    <th>SL</th>
                    <th>Subject Name</th>
                    <th>Subject Type</th>
                  </tr>
                </thead>
                <tbody>
                 @foreach ($getRecord as $key => $value)
                 <tr>
                    <th>{{ ++$key }}</th>
                    <th>{{ $value->subject_name }}</th>
                    <th>{{ $value->subject_type }}</th>
                 </tr>
                 @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>

@endsection
@extends('layouts.app')

@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>My Account</h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            @include('_message')
            <div class="card card-primary">
              <form method="post" action="" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label>First Name <span style="color: red;">*</span></label>
                            <input type="text" class="form-control" name="name" value="{{ old('name', $getRecord->name) }}">
                            <div style="color: red">{{$errors->first('name')}}</div>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Last Name <span style="color: red;">*</span></label>
                            <input type="text" class="form-control" name="last_name" value="{{ old('last_name', $getRecord->last_name) }}">
                            <div style="color: red">{{$errors->first('last_name')}}</div>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Admission Number <span style="color: red;">*</span></label>
                            <input type="text" class="form-control" name="admission_number" value="{{ old('admission_number', $getRecord->admission_number) }}" readonly>
                            <div style="color: red">{{$errors->first('admission_number')}}</div>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Roll Number <span style="color: red;">*</span></label>
                            <input type="text" class="form-control" name="roll_number" value="{{ old('roll_number', $getRecord->roll_number) }}" readonly>
                            <div style="color: red">{{$errors->first('roll_number')}}</div>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Gender <span style="color: red;">*</span></label>
                            <select class="form-control" required name="gender">
                                <option value="">Select Gender</option>
                                <option {{ (old('gender', $getRecord->gender) == 'Male') ? 'selected' : '' }} value="Male">Male</option>
                                <option {{ (old('gender', $getRecord->gender) == 'Female') ? 'selected' : '' }} value="Female">Female</option>
                                <option {{ (old('gender', $getRecord->gender) == 'Other') ? 'selected' : '' }} value="Other">Other</option>
                            </select>
                            <div style="color: red">{{$errors->first('gender')}}</div>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Date of Birth<span style="color: red;">*</span></label>
                            <input type="date" class="form-control" value="{{ old('date_of_birth', $getRecord->date_of_birth) }}"  name="date_of_birth">
                            <div style="color: red">{{$errors->first('date_of_birth')}}</div>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Caste <span style="color: red;">*</span></label>
                            <input type="text" class="form-control" value="{{ old('caste', $getRecord->caste) }}" name="caste">
                            <div style="color: red">{{$errors->first('caste')}}</div>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Religion <span style="color: red;">*</span></label>
                            <input type="text" class="form-control" name="religion" value="{{ old('religion', $getRecord->religion) }}">
                            <div style="color: red">{{$errors->first('religion')}}</div>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Mobile Number <span style="color: red;">*</span></label>
                            <input type="text" class="form-control" value="{{ old('mobile_number', $getRecord->mobile_number) }}" name="mobile_number">
                            <div style="color: red">{{$errors->first('mobile_number')}}</div>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Admission Date<span style="color: red;">*</span></label>
                            <input type="date" class="form-control" value="{{ old('admission_date', $getRecord->admission_date) }}" name="admission_date" readonly>
                            <div style="color: red">{{$errors->first('admission_date')}}</div>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Profile Picture <span style="color: red;">*</span></label>
                            <input type="file" class="form-control" name="profile_pic">
                            <div style="color: red">{{$errors->first('profile_pic')}}</div>
                            @if (!empty($getRecord->getProfile()))
                                <img src="{{ $getRecord->getProfile() }}" style="width: 100px;">
                            @endif
                        </div>
                        <div class="form-group col-md-6">
                            <label>Blood Group <span style="color: red;">*</span></label>
                            <input type="text" class="form-control" name="blood_group" value="{{ old('blood_group', $getRecord->blood_group) }}" placeholder="Enter Blood Group">
                            <div style="color: red">{{$errors->first('blood_group')}}</div>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Height</label>
                            <input type="text" class="form-control" name="height" value="{{ old('height', $getRecord->height) }}" placeholder="Enter Height">
                            <div style="color: red">{{$errors->first('height')}}</div>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Weight</label>
                            <input type="text" class="form-control" name="weight" value="{{ old('weight', $getRecord->weight) }}" placeholder="Enter Weight">
                            <div style="color: red">{{$errors->first('weight')}}</div>
                        </div>
                    </div>  
                  
                  <div class="form-group">
                    <label>Email <span style="color: red;">*</span></label>
                    <input type="email" class="form-control"  name="email"  value="{{ old('email', $getRecord->email) }}" readonly>
                    <div style="color: red">{{$errors->first('email')}}</div>
                  </div>
                </div>
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

@endsection
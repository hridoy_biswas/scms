@extends('layouts.app')

@section('content')

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Parent (Total: {{$getRecord->total()}})</h1>
        </div>
        <div class="col-sm-6" style="text-align: right">
          <a href="{{ url('admin/parent/add') }}" class="btn btn-primary">Add New Parent</a>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">

          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Search Parent</h3>
            </div>
            <form method="get" action="">
              <div class="card-body">
                <div class="row">
                    <div class="form-group col-md-2">
                      <label>Name</label>
                      <input type="text" class="form-control" name="name" value="{{ Request::get('name') }}" placeholder="Enter Name">
                    </div>
                    <div class="form-group col-md-2">
                      <label>Last Name</label>
                      <input type="text" class="form-control" name="last_name" value="{{ Request::get('last_name') }}" placeholder="Enter Last Name">
                    </div>
                    <div class="form-group col-md-2">
                      <label>Email</label>
                      <input type="text" class="form-control" name="email" value="{{ Request::get('email') }}" placeholder="Enter Email">
                    </div>
                    <div class="form-group col-md-2">
                        <label>Mobile Number</label>
                        <input type="text" class="form-control" name="mobile_number" value="{{ Request::get('mobile_number') }}" placeholder="Enter Mobile Number">
                      </div>
                    <div class="form-group col-md-2">
                    <label>Occupation</label>
                        <input type="text" class="form-control" name="occupation" value="{{ Request::get('occupation') }}" placeholder="Enter Occupation">
                    </div>
                    <div class="form-group col-md-2">
                        <label>Address</label>
                        <input type="text" class="form-control" name="address" value="{{ Request::get('address') }}" placeholder="Enter Address">
                    </div>
                    <div class="form-group col-md-2">
                      <label>Gender</label>
                      <select class="form-control" name="gender">
                        <option value="">Select Gender</option>
                        <option {{ (Request::get('gender') == 'Male') ? 'selected' : '' }} value="Male">Male</option>
                        <option {{ (Request::get('gender') == 'Female') ? 'selected' : '' }} value="Female">Female</option>
                        <option {{ (Request::get('gender') == 'Other') ? 'selected' : '' }} value="Other">Other</option>
                    </select>
                    </div>
                    <div class="form-group col-md-2">
                      <label>Status</label>
                      <select class="form-control" name="status" >
                          <option value="">Select Status</option>
                          <option {{ (Request::get('status') == 100) ? 'selected' : '' }} value="100">Active</option>
                          <option {{ (Request::get('status') == 1) ? 'selected' : '' }} value="1">Inactive</option>
                      </select>
                    </div>
                    <div class="form-group col-md-2">
                      <label>Created Date</label>
                      <input type="date" class="form-control" name="created_at" value="{{ Request::get('created_at') }}">
                    </div>
                    <div class="form-group col-md-3">
                      <button class="btn btn-primary" type="submit" style="margin-top: 30px;">Search</button>
                      <a href="{{ url('admin/parent/list') }}" class="btn btn-success" type="submit" style="margin-top: 30px;">Reset</a>
                    </div>
                  </div>
              </div>  
            </form>
          </div>


          @include('_message')
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Parent List</h3>
            </div>
            <div class="card-body p-0">
              <table class="table table-striped">
                <thead style="margin-top: 500px;">
                  <tr>
                    <th>SL</th>
                    <th>Profile Picture</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Gender</th>
                    <th>Mobile Number</th>
                    <th>Occupation</th>
                    <th>Address</th>
                    <th>Status</th>
                    <th>Created Date</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($getRecord as $key => $value)
                  <tr>
                    <td>{{ ++$key }}</td>
                    <td>
                        @if (!empty($value->getProfile()))
                            <img src="{{ $value->getProfile() }}" style="height: 50px; width: 50px; border-radius: 50px;">                        
                        @endif
                      </td>
                    <td>{{ $value->name }}</td>
                    <td>{{ $value->email }}</td>
                    <td>{{ $value->gender }}</td>
                    <td>{{ $value->mobile_number }}</td>
                    <td>{{ $value->occupation }}</td>
                    <td>{{ $value->address }}</td>
                    <td>{{ ($value->status == 0) ? 'Active' : 'InActive' }}</td>
                    <td>{{ date('d-m-Y H:i A', strtotime($value->created_at)) }}</td>
                    <td style="min-width: 150px;">
                      <a href="{{ url('admin/parent/edit/'.$value->id) }}" class="btn btn-primary btn-sm">Edit</a>
                      <a href="{{ url('admin/parent/delete/'.$value->id) }}" class="btn btn-danger btn-sm">Delete</a>
                      <a href="{{ url('admin/parent/my-student/'.$value->id) }}" class="btn btn-primary btn-sm" style="margin-top: 5px;">My Student</a>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            <div style="padding: 20px; float:right;">
              {!! $getRecord->appends(Illuminate\Support\Facades\Request::except('page'))->links() !!}
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>

@endsection